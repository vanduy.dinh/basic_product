import React ,{useEffect} from 'react';
import { Card, Col, Row, Image , Spin} from 'antd';
import http from '../http';
import { useState } from 'react';
export default function PROD({items}){
  const [loading2 , setLoading2] = useState(false)

  console.log("data from image",items.id_pro)
  const [getDev , setGetDev] = useState([])
  useEffect(()=>{
    var someValue = window.sessionStorage.getItem('user');
    var obj = JSON.parse(someValue);
    var id = obj.id;
    fetchdev(id);
  },[]);
//TABLE
//END TABLE
  const fetchdev = (id)=>{
    setLoading2(true)
    http.get('/detailsclient/'+id).then(res=>{
          let data = res.data.filter(index =>(index.version == "prod") )
          let data2 = data.filter(index2=>index2.product_id == items.id_pro)
          
          setGetDev([...data2])
    setLoading2(false)
    })
  }
    return( 
      <Spin spinning={loading2}>
      
      <div className="site-card-wrapper">
      <Row gutter={6} style={{width:"400px", height:"300px"}}> 
      {
       getDev.map((data, index)=>{
        if(data === null){
          console.log("null")
        }
       return (
        <Col span={12}  key={index} >
          <Card title="Prod ..." bordered={true}>
          <ul className='list-group'>
          {/* <li  className="list-group-item  justify-align-center " >
          <Image src={"http://localhost/laravel/reactjsnew/hehehe/public/images/"+ data.image} width={35} height={35}/>
          </li> */}
            <li  className="list-group-item" > {data.link}</li>
            <li  className="list-group-item">{data.userpass}</li>
            <li  className="list-group-item"> {data.product_id}</li>
          </ul>
          </Card>
        </Col>
        
       )
       })
      }

        
       
      </Row>
    </div>
    </Spin >

    )
  

  
}