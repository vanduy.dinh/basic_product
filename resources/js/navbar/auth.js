import React, { useState } from 'react';
import { Routes, Route, Link , useNavigate, Navigate ,  Select} from 'react-router-dom';
import AuthUser from '../components/AuthUser';
import Dashboard from '../components/Dashboard';
import AllProduct from '../components/Allproduct';
import ClientView from '../components/Clientview';
import AddDetail from '../components/AddDetails';
import AllProductdetails from '../components/Allproductdetails';
import Auth_Client from './auth_client';
import CreatePro from '../components/CreateProd';
import Editprod from '../components/Editprod';
import Editdetails from '../components/Editdetails';
import EditUser  from '../components/EditUser';
import EditRole  from '../components/editRole';
import Logout from '../components/LogOut';
import { Button, Modal } from 'antd';
import AddDetails from '../components/AddDetails';
import DashTeam1 from '../cms/team1/dashteam1';
 import Trash from '../cms/trashS'; 
import { Col, Divider, Row} from 'antd';
import {
  DesktopOutlined,
  FileOutlined,
  PieChartOutlined,
  TeamOutlined,
  LogoutOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Breadcrumb, Layout, Menu } from 'antd';
const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}

const items = [
  getItem('Dashboard', '/dashboard', <PieChartOutlined />),
  // getItem('Client View Test', '/clientview', <DesktopOutlined />),
  getItem('Product', 'sub1', <UserOutlined />, [
    getItem('Allproducts', '/allproduct'),
    getItem('Allproductsdetails', '/allproductdetails'),
  ]),
  getItem('Modules', 'sub2', <TeamOutlined />, [
    getItem('Team 1', '/team1 '),
     getItem('Team 2', '/team2'),
    ]),
  getItem('Logout','/logout',<LogoutOutlined />),
];
const Auth = () => {
  //get token 
  var someValue = window.sessionStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var quyen = obj.phanquyen;
  var email = obj.email
  const [isModalOpen, setIsModalOpen] = useState(false);
const showModal = () => {
  setIsModalOpen(true);
};
const handleOk = () => {
  setIsModalOpen(false);
  logoutUser()
};
const handleCancel = () => {
  setIsModalOpen(false);
};
const [collapsed, setCollapsed] = useState(false);
const navigate = useNavigate();
 const {token,logout} = AuthUser();
    const logoutUser = () => {
        if(token != undefined){
            logout();
        }
    }

    if( quyen == "0"){
      return (
        <Layout
          style={{
            minHeight: '100vh',
          }}
        >
          <Sider   collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
            <div className="logo" />
            <Menu  onClick={({key})=>{
             navigate(key);
           }}  theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items}  />
  
          </Sider>
          <Layout className="site-layout">
            <Header
              className="site-layout-background"
              style={{
                padding: 0,
              }}
            />
            <Content
              style={{
                margin: '0 16px',
              }}
            >
              <div
                className="site-layout-background"
                style={{
                  padding: 24,
                  minHeight: 360,
                }}
              >
         <ContentRender />
              </div>
            </Content>
            <Footer
              style={{
                textAlign: 'center',
              }}
            >
            </Footer>
          </Layout>
          <>
          <Modal title="Đăng Xuất" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
            <p> Bạn có chắc chắn đăng xuất </p>
          </Modal>
          <div className="hehehehe">
                <nav className="navbar navbar-expand-sm navbar- hehehehe">
                   <ul className="navbar-nav hehehehe ">
                      <li className="nav-item hehehehe">
                      <p role="button" className="nav-link logout" onClick={showModal}>  <UserOutlined  className='logo2' /> </p>
                    </li>
                  </ul>
                </nav>
              <div>
              </div>
          </div>
          </>
        </Layout>
      );
    }
    return(
      <div>
        <Auth_Client/>
      </div>
     
    )
  
};
function ContentRender(){
   
    return (
      <div>
      <Routes>
     <Route path="/dashboard" element={<Dashboard />} />
     <Route path="/allproduct" element={<AllProduct />} /> 
     <Route path="/clientview" element={<ClientView />} /> 
     <Route path="/addDetails" element={<AddDetail />} /> 
     <Route path="/allproductdetails" element={<AllProductdetails />} /> 
     <Route path="/CreateDetails/:id" element={<AddDetail />} />
     <Route path="/CreatePro" element={<CreatePro />} />
     <Route path="/Editprod/:id_pro" element={<Editprod />} />
     <Route path="/Editdetails/:id_pro" element={<Editdetails />} />
     <Route path="/editUser/:id_pro" element={<EditUser />} />
     <Route path="/team1" element={<DashTeam1 />} />

     <Route path="/ThungRac" element={<Trash/>} />
     {/* <Route path="/CreateDetails/:id_pro" element={<AddDetails />} /> */}
     <Route path="/role_ver/:id" element={<EditRole />} />
     <Route path="/logout" element={<Logout />} />
     {/* roleVersion */}
     {/* <Route path="/Editdetails/:id_pro" element={<Editdetails />} /> */}
     </Routes>
      </div>
    )
}
export default Auth;