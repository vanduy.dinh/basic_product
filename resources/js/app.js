require('./bootstrap');
import { useState , createContext } from 'react';
import React from 'react';
import ReactDOM from 'react-dom';
import { Routes, Route, browserHistory, BrowserRouter  } from 'react-router-dom';
import '../css/app.css'
import AuthUser from './components/AuthUser';
import Guest from './navbar/guest';
import Auth from './navbar/auth';
import Auth_Client from './navbar/auth_client';

export const themeContext = createContext()

function App () {
  const {getToken} = AuthUser();
  // console.log(getToken)


  
  if(!getToken()){
    return <Guest />
  }
    return (
<div>
<Auth/>

</div>    );
  
 

}


ReactDOM.render( 
    <BrowserRouter>
    <App />
    </BrowserRouter>,
    document.getElementById('example')
  );
