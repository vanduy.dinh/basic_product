import { useEffect, useState } from "react";
import { useNavigate , useParams} from "react-router-dom";
import http from '../http'
import React from 'react';
import { Button, Form, Input,Image ,Select } from 'antd';
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
const EditUser = () => {
const [form] = Form.useForm();
const navigate = useNavigate();
// const [oneProduct , setOneProduct] = useState([]);
let {id_pro} =useParams();
const [inputs,setInputs] = useState({});
const [dataone,setDataOne] = useState({});

const hinh = "http://localhost/laravel/reactjsnew/hehehe/public/images/"+dataone.icon
const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}

const fetchOne = ()=>{
    http.get('/users/'+id_pro+'/edit').then(res=>{
      res.data.password = ''

      form.setFieldsValue(res.data)

      setDataOne(res.data);
      setInputs({
        name:res.data.name,
        email:res.data.email,
        password: res.data.password,
        phanquyen:res.data.phanquyen,
      })
    }
    )
}
useEffect(()=>{
    fetchOne();
},[])

const onFinish = (inputs) => {
    http.put('/users/'+id_pro,inputs).then((res)=>{
        navigate('/dashboard');
    })  
    setInputs(inputs) ;
  }
return (
    <Form {...layout}  form={form}  dataSource={dataone}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
    <h2>Edit Product </h2>
      <Form.Item
        name="name" 
        label="Name"  
        rules={[
            {
              required: true,
            },
          ]}
          >
        <Input   value={inputs.name || ''} onChange={handleChange} />
      </Form.Item>
      <Form.Item
        name="email" 
        label="Email" 
        rules={[
            {
              required: true,
            },
          ]}
          >
        <Input disabled="true"  value={inputs.email || ''} onChange={handleChange} />
      </Form.Item>
      <Form.Item
        name="password" 
        label="Password"  
        rules={[
            {
              required: false,
            },
          ]}
          >
        <Input   value={inputs.password || ''} onChange={handleChange} />
      </Form.Item>
      <Form.Item label="Vai Trò"
      name="phanquyen">
          <Select
           value={inputs.phanquyen || ''} onChange={handleChange} 
        >
            <Select.Option  value="1">Admin</Select.Option>
            <Select.Option value="0">User</Select.Option>
           

          </Select>
        </Form.Item>
        {/* <Form.Item label="Chức Năng"
          name="role">
          <Select
           value={inputs.role || ''} onChange={handleChange} 
        >
            <Select.Option  value="dev">DEV</Select.Option>
            <Select.Option value="prod">PROD</Select.Option>
          </Select>
        </Form.Item> */}
      <Form.Item
        wrapperCol={{
          ...layout.wrapperCol,
          offset: 8,
        }}
      >
        <Button type="primary"  htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form >
  );
};
export default EditUser;
