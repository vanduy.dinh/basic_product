import Axios from "axios";
import { useEffect, useState  } from "react";
import { useNavigate,useParams } from "react-router-dom";
import { UploadOutlined } from '@ant-design/icons';

import React from 'react';
import { Button, Form, Input, Select } from 'antd';
import {  message, Upload } from 'antd';
import http from "../http";
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16
  },
};
/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
/* eslint-enable no-template-curly-in-string */

const AddDetails = () => {
const [form] = Form.useForm();

const navigate = useNavigate();
const {id} = useParams([]);
// console.log(id)
form.setFieldValue("product_id",id)

// console.log("dddd",oldData.link)
// const props = {
//   name: 'file',
//   action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
//   headers: {
//     authorization: 'authorization-text',
//   },

//   onChange(info) {
//     if (info.file.status !== 'uploading') {
//       console.log(info.file, info.fileList);
//     }
//     if (info.file.status === 'done') {
//       message.success(`${info.file.name} file uploaded successfully`);
//     } else if (info.file.status === 'error') {
//       message.error(`${info.file.name} file upload failed.`);
//     }
//   },
// };
const [inputs,setInputs] = useState({});
const [imagedata, setImagedata] = useState({});
const [file, setFile] = useState({});

const [oldData , setOldata] = useState([])
  const handleChangeimage = (file) =>{
      setImagedata(file[0]);
    };
const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}

useEffect(()=>{
  fetchOneByid();

},[]);
let datafil
const fetchOneByid=()=>{
  http.get('/CreateDetails/'+id )
  .then(res=>{
    // datafil = res.data.filter(index =>(index.version == "dev" ))

    datafil =res.data.map(index=>{
      // console.log(index)
      setOldata({
        link:index.link,
        version:index.version,
        userpass:index.userpass,
      })
    })
    // console.log("filter data", datafil.link);
  })
}
form.setFieldValue("link",oldData.link)
form.setFieldValue("version",oldData.version)
// form.setFieldValue("image","1671077148.png")
form.setFieldValue("userpass",oldData.userpass)


console.log("concatmeo",oldData.link)
// console.log("old",oldData.filter(items=>{
//   items.link
// }))
const onFinishimage = () => {
  let formDaTa = new FormData;
  formDaTa.append("image",imagedata);
  // console.log(imagedata.name)
    Axios.post("http://localhost:8000/api/upload",formDaTa)
  .then(res => {
  })
}
  const onFinish = (values) => {
    let formDaTa = new FormData;
    formDaTa.append("link",values.link);
    formDaTa.append("version",values.version);
    formDaTa.append("userpass",values.userpass);
    formDaTa.append("product_id",values.product_id);
    // formDaTa.append("image",imagedata);
    formDaTa.append("image",file);

    console.log(imagedata)
    // console.log(values);
    Axios.post("http://localhost:8000/api/productdetails",formDaTa)
    .then(res => {
      // console.log(formDaTa)
      navigate('/allproductdetails');
    })
  }
  const props = {
   
    beforeUpload: (file) => {
      setFile(file)
      console.log(file.type)
      const isPNG = file.type === 'image/jpeg';
      if (!isPNG) {
        message.error(`${file.name} is not a jpg file`);
      }
      return isPNG || Upload.LIST_IGNORE;
    },
    onChange: (info) => {
    },
  };
  if(oldData.length >= 1 ){
    return (
      <Form {...layout}  form={form}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
      <h2>Override Product Details </h2>
      <Form.Item
          name="link" 
          label="Link"
          rules={[
              {
                required: true,
              },
            ]}
            >
          <Input  />
        </Form.Item>
        <Form.Item
          name="userpass" 
          label="userpass"
          rules={[
              {
                required: true,
              },
            ]}
            >
          <Input value="" />
        </Form.Item>
  
        <Form.Item label="Version "
        name="version">
          <Select>
            <Select.Option value="dev">Dev</Select.Option>
            <Select.Option value="prod">Prod</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
           
          name="product_id"
          label="Pro ID"
          
          rules={[
              {
                required: true,
              },
            ]}
        >
          <Input disabled={true}  />
        </Form.Item>
        <Form.Item
        name="image"
        label="Image">
          <Input type="file" name="image" onChange={e =>handleChangeimage(e.target.files)} id="image" />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            ...layout.wrapperCol,
            offset: 8,
          }}
        >
          <Form.Item>
          </Form.Item>
          <Button type="primary"  htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }else{
    return(
      <Form {...layout}  form={form}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
      <h2>New Product Details </h2>
      <Form.Item
          name="link" 
          label="Link"
          rules={[
              {
                required: true,
              },
            ]}
            >
          <Input  />
        </Form.Item>
        <Form.Item
          name="userpass" 
          label="userpass"
          rules={[
              {
                required: true,
              },
            ]}
            >
          <Input value="" />
        </Form.Item>
        <Form.Item label="Version "
        name="version">
          <Select>
            <Select.Option value="dev">Dev</Select.Option>
            <Select.Option value="prod">Prod</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
         
          name="product_id"
          label="Pro ID"
          rules={[
              {
                required: true,
              },
            ]}
        >
          <Input disabled="true"  />
        </Form.Item>
        {/* <Form.Item
        name="image"
        label="Image">
          
          <Input type="file" name="image"  onChange={e =>handleChangeimage(e.target.files)} id="image" />
        </Form.Item> */}
        <Form.Item
         name="image"
         label="Image"
        >
          <Form.Item>
          <Upload {...props} >
            <Button name=" image" id="image" onChange={e =>handleChangeimage(e.target.files)} icon={<UploadOutlined />}>Upload</Button>
          </Upload>
          </Form.Item>
          <Button type="primary"  htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
  
};
export default AddDetails;
