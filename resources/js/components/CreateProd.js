
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import http from '../http'
import React from 'react';
import { Button, Form, Input, InputNumber } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import {  message, Upload } from 'antd';
import { ToastContainer, toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
/* eslint-enable no-template-curly-in-string */

const CreatePro = () => {
  var someValue = window.sessionStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var id_user = obj.id;
const [form] = Form.useForm();
form.setFieldValue("icon","2.png")

const navigate = useNavigate();
const props = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
const [inputs,setInputs] = useState({});
const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}

  const onFinish = (values) => {
    
    const form = new FormData();
    form.append('id_user',id_user);
    form.append('icon',values.icon)
    form.append('name_pro',values.name_pro)
    form.append('phanloai',values.phanloai)
    form.append('mota',values.mota)
    http.post('/products',form).then((res)=>{
      if(res.data == "user khong co quyen nay"){
        toast.error("User không có quyền này!",{
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      }else{
        navigate('/allproduct')
      }
    })
    setInputs(values) ;
  }
  return (
    <Form {...layout}  form={form}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
    <h2>Thêm Product </h2>
    <ToastContainer />
      <Form.Item
        name="name_pro" 
        label="Name"
        rules={[
            {
              required: true,
            },
          ]}
          >
        <Input />
      </Form.Item>
      <Form.Item
        name="phanloai"
        label="Phân Loại"
        rules={[
            {
              required: true,
            },
          ]}
        
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='mota'
        label="Mô Tả"
       
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="icon" 
        label="ICON"
        rules={[
            {
              required: true,
            },
          ]}
          >
        <Input hidden={true} />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          ...layout.wrapperCol,
          offset: 8,
        }}
      >

        <Upload {...props}>
    <Button icon={<UploadOutlined />}>Click to Upload</Button>

  </Upload>
        <div className="btn-add">
        <Button  type="primary"  htmlType="submit">
          Submit
        </Button>
        </div>
  </Form.Item>

    </Form>
  );
};
export default CreatePro;
