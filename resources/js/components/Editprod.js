
import { useEffect, useState } from "react";
import { UploadOutlined } from '@ant-design/icons';
import { useNavigate , useParams} from "react-router-dom";
import http from '../http'
import React from 'react';
import { Button, Form, Input,Image, Upload } from 'antd';
import TextArea from "antd/es/input/TextArea";
import { toast } from "react-toastify";

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
const Editprod = () => {
  var someValue = window.sessionStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var id_user = obj.id;
  console.log("dddd",id_user)
const [form] = Form.useForm();
const [file, setFile] = useState({});

const navigate = useNavigate();
// const [oneProduct , setOneProduct] = useState([]);
let {id_pro} =useParams();
const [inputs,setInputs] = useState({});
const [dataone,setDataOne] = useState({});
console.log()

const hinh = "http://localhost/laravel/reactjsnew/hehehe/public/images/"+dataone.icon
const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}

const props = {
  beforeUpload: (file) => {
    setFile(file)
    const isPNG = file.type === 'image/cc' ;
    if (!isPNG) {
      message.error(`${file.name} is not a jpg file`);
    }
    return isPNG || Upload.LIST_IGNORE;
  },
  onChange: (info) => {
  },
};
const fetchOne = ()=>{
    http.get('/products/'+id_pro+'/edit').then(res=>{
      form.setFieldsValue(res.data)
      console.log("res.data",res.data)
      setDataOne(res.data);
      setInputs({
        id_pro:res.data.id_pro,
        name_pro:res.data.name_pro,
        phanloai:res.data.phanloai,
        mota:res.data.mota,
      })
    }
    )
}
useEffect(()=>{
    fetchOne();
},[])

const onFinish = (inputs) => {
    // console.log(inputs);
    inputs.icon = file.name
    inputs.id_user = id_user
    http.put('/products/'+id_pro,inputs).then((res)=>{
      if(res.data == "Chưa thay đổi hình"){
        toast.error("Lỗi !")
      } else{
        navigate('/allproduct');
      }
    })  
    setInputs(inputs) ;
  }
  return (
    <Form {...layout}  form={form}  dataSource={dataone}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
    <h2>Edit Product </h2>
    <ToastContainer />
      <Form.Item
        name="name_pro" 
        label="Name"  
        rules={[
            {
              required: true,
            },
          ]}
          >
        <Input   value={inputs.name_pro || ''}
      onChange={handleChange} />
      </Form.Item>
      <Form.Item
        name="phanloai"
        label="Phân Loại"
        rules={[
            {
              required: true,
            },
          ]}
      >
        <Input value={inputs.phanloai || ''}
                                onChange={handleChange}  />
      </Form.Item>
      <Form.Item
        name='mota'
        label="Mô Tả"
      >
        {/* <Input value={inputs.mota || ''}
         onChange={handleChange}  /> */}
       <TextArea value={inputs.mota || ''}
        onChange={handleChange}  >
     </TextArea>
      </Form.Item>
      <Form.Item
        name="icon"
        label="Icon"
      >
       <Image src={hinh} width={100} height={100} />
      </Form.Item>
      <Form.Item
        label="Thay Icon "
      >
          <Upload {...props} >
            <Button name="icon" id="icon" icon={<UploadOutlined />}>Thay Đổi</Button>
          </Upload>
      </Form.Item>
      <Form.Item
        wrapperCol={{
          ...layout.wrapperCol,
          offset: 8,
        }}
      >
        <Button type="primary"  htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form >
  );
};
export default Editprod;
