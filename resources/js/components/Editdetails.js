
import { useEffect, useState } from "react";
import { UploadOutlined } from '@ant-design/icons';

import { useNavigate , useParams} from "react-router-dom";
import http from '../http'
import React from 'react';
import { Button, Form, Input,Image,Select , Upload} from 'antd';
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
const Editdetails = () => {
  var someValue = window.sessionStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var id_user = obj.id;
const [form] = Form.useForm();
const navigate = useNavigate();
// const [oneProduct , setOneProduct] = useState([]);
let {id_pro} =useParams();
let id_prodetail = id_pro;
const [inputs,setInputs] = useState({});
const [file, setFile] = useState({});

const [dataone,setDataOne] = useState({});
const hinh = "http://localhost/laravel/reactjsnew/hehehe/public/images/"+dataone 
const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}
  const props = {
    beforeUpload: (file) => {
      setFile(file)
      const isPNG = file.type === 'image/cc' ;
      if (!isPNG) {
        message.error(`${file.name} is not a jpg file`);
      }
      return isPNG || Upload.LIST_IGNORE;
    },
    onChange: (info) => {
    },
  };
  // const hehehe= () =>{
  //   onChangeImage();
  //   let namefile= file.name;
  //   http.put('/productdetails/'+id_prodetail,namefile).then((res)=>{
  //     navigate('/allproductdetails');
  // }) 

    
  // }
  const handleChangeimage = (file) =>{
    setImagedata(file[0]);
  };
const fetchOne = ()=>{ 
  
    http.get('/productdetails/'+id_prodetail+'/edit').then(res=>{
      form.setFieldsValue(res.data)
      setDataOne(res.data.image);

      setInputs({
        id_prodetail:res.data.id_prodetail,
        link:res.data.link,
        userpass:res.data.userpass,
        version:res.data.version,
      })
    }
    )
}
useEffect(()=>{
    fetchOne();
},[])
// setInputs({
//   image:file.name
// })


const onChangeImage = () =>{
  console.log("fileeee",file.name)

}
const onFinish = (inputs) => {
  if(file.name ==  null){
    http.put('/productdetails/'+id_prodetail,inputs).then((res)=>{
      navigate('/allproductdetails');
  }) 
  }else{
    inputs.image = file.name
    http.put('/productdetails/'+id_prodetail,inputs).then((res)=>{
      navigate('/allproductdetails');
  })  

  }  
   
    setInputs(inputs) ;
  }
  return (
    <Form {...layout}  form={form}  dataSource={dataone}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
    <h2>Edit Product </h2>
    <Form.Item
        name="id_prodetail" 
        label="ID"  
        rules={[
            {
              required: true,
            },
          ]}
          >
        <Input disabled="true"  value={inputs.id_productdetai || ''}
              onChange={handleChange} />
      </Form.Item>
      <Form.Item
        name="link" 
        label="Link"  
        rules={[
            {
              required: true,
            },
          ]}
          >
        <Input   value={inputs.link || ''}
              onChange={handleChange} />
      </Form.Item>
      <Form.Item
        name="userpass"
        label="USER PASS"
        rules={[
            {
              required: true,
            },
          ]}
      >
        <Input value={inputs.userpass || ''}
             onChange={handleChange}  />
      </Form.Item>
      <Form.Item label="Version"
      name="version">
          <Select
           value={inputs.version || ''} onChange={handleChange} 
        >
            <Select.Option  value="dev">DEV</Select.Option>
            <Select.Option value="prod">PROD</Select.Option>
          </Select>
        </Form.Item>
        <Input  type="hidden" disabled="true" value={inputs.product_id || ''}
             onChange={handleChange}  />
      <Form.Item
        name="image"
        label="Icon"
      >
       <Image src={hinh} width={100} height={100} />
      </Form.Item>
      <Form.Item
        label="Thay Icon "
      >
          <Upload {...props} >
            <Button name=" image" id="image" icon={<UploadOutlined />}>Thay Đổi</Button>
          </Upload>
      </Form.Item>
      <Form.Item
        wrapperCol={{
          ...layout.wrapperCol,
          offset: 8,
        }}
      >
        <Button type="primary"    htmlType="submit">
          Submit
        </Button>
        {/* <Button type="primary" htmlType="submit">
          Submit
        </Button> */}
      </Form.Item>
    </Form >
  );
};
export default Editdetails;
