import React, { useState , useEffect } from 'react';
import { Button, Table, Space, Spin,Modal  , Image} from 'antd';
import http from "../http";
import { useNavigate } from "react-router-dom";
import { Col, Drawer, Form, Input, Row, Select } from 'antd';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { xor } from 'lodash';
const { Option } = Select;
const initPagination = {
  current: 1,
  defaultCurrent: 1,
  total: 20,
  defaultPageSize: 4,
  showSizeChanger: false
}

const AllProduct = () => {
const navigate = useNavigate();  
var someValue = window.sessionStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var id_user = obj.id;
const [childrenDrawer, setChildrenDrawer] = useState(false);
const [loading2 , setLoading2] = useState(false)
const [open, setOpen] = useState(false);
const [inputs,setInputs] = useState({});
const [inputs2,setInputs2] = useState({});


const [layId,setLayId] = useState([]);
const showDrawer = (id_pro) => {
  inputs.email = id_pro.id
  console.log("inputs.email",inputs.email)
  setInputs2({
    email:id_pro.id,
  })
  setOpen(true);
};


const onClose = () => {
  setOpen(false);
};
const [form] = Form.useForm();
  const [current, seturlPage] = useState([]);
  const [product, setProduct] = useState([]);
  const [productdetails, setProductdetails] = useState([]);
  const [users, setUsers] = useState([]);
  const [idPro, setIdPro] = useState("");
  const [layid,setLayddsId] = useState([]);
  const [pagination, setPaginate2] = useState(initPagination);
 const edituser = (id_pro) =>{
  navigate('/editUser/'+id_pro.id);
 }


 const rolepermissions = (id_pro) =>{
  navigate('/role_ver/'+id_pro.id);
 }


  const submitX=(id_prodetail) =>{

    showChildrenDrawer()
  }
  const sumbitXem = (id_pro) =>{
    setIdPro(id_pro.id_pro)
    form.setFieldsValue(id_pro)
    showDrawer();
    
  }


  const handletablechange2 = (pagination) => {
    console.log(pagination.current);
    let p = {
      current: pagination.current,
      defaultCurrent: 1,
      total: 10,
      defaultPageSize: 5,
      showSizeChanger: false
    };
    setPaginate2({...p})
    fetchAllPro(pagination.current);
  };
 

  const deleteProduct = (id_pro) => {

    toast("Xóa Thành Công!");
    console.log("SSS", id_pro.id)
    http.delete('/users/'+id_pro.id).then(res=>{
      fetchAllPro();
    })
    .catch((err) =>{
    toast("Không Thành Công!");
    })
  }
//EDIT
const handleChange = (event) => {
  const name = event.target.name;
  const value = event.target.value;
  setInputs(values => ({...values,[name]:value}))
}
  const onFinish =(inputs)=>{
    inputs.name = id_user;
    console.log("inputs2",inputs2)
    http.put('/users_role/'+inputs2.email,inputs).then((res)=>{
      if(res.data != "khong co quyen super_admin"){
        onClose()

      }else{
        toast.error("Không có quyền !!")
      }
    })  
    setInputs(inputs) ;
    // http.put('/products/update/'+layid,inputs).then((res)=>{
    //   navigate('/allproduct');
    // })
  }


//END EDIT
  const columns = [
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Permission',
      dataIndex: 'phanquyen',
      render: (phanquyen, record) => {
        if(phanquyen == "0"){
          return (
            <p className='Admincss'> ADMIN</p>
          );
        } else{
          return (
            <p className='Admincss2'> USER</p>
          );
        }
       
      }
    },
    {
      title: 'Action',
      key: 'action',
      render: (id_pro, record) => (
        <Space size="middle">
          <a  className='suadas' onClick={()=> {edituser(id_pro)}}>Sửa</a>
          <a  className='suadas' onClick={() => showDrawer(id_pro)}>Permission</a>
          <a className='suadas' onClick={()=> {rolepermissions(id_pro)}}>Cấp Quyền</a>
          <a className='capquyen' onClick={()=>{deleteProduct(record)}} >Xóa</a>
        </Space>
      ),
    },
  ];

  useEffect(()=>{
    fetchAllPro();
   // fetchPro();
  },[]);

  
  const fetchAllPro = (current) => {
    setLoading2(true)
    http.get('/users'+'?page='+current).then(res=>{
      res.data.data.map(items=>{
        // console.log("map items",items) 
      })
        setProduct([...res.data.data]);
      setLoading2(false)
    })
    // })
}

const showChildrenDrawer = (id_pro) => {
  navigate("/allproductdetails")
  setChildrenDrawer(true);
};


const onChildrenDrawerClose = () => {
  setChildrenDrawer(false);
};
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const start = () => {
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      
    }, 1000);
  };
  const onSelectChange = (newSelectedRowKeys) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        <h2>DASH BOARD</h2>
        <div
          style={{
            marginBottom: 16,
          }}
        >
          <span
            style={{
              marginLeft: 8,
            }}
          >
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Drawer
          dataSource={productdetails}
          title="PHÂN QUYỀN"
          width={720}
          onClose={onClose}
          open={open}
          bodyStyle={{
            paddingBottom: 80,
          }}
          extra={
            <Space>
              <Button onClick={onClose}>Cancel</Button>
            </Space>
          }
        >
          <Form form={form} layout="vertical"  onFinish={onFinish} >
          <Image className='KKK'
              width={150}
              src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC6iPDSqcgCcAtdEz_rPY0B-sxqMd7hz0Hlg&usqp=CAU"}
          />
            <Row gutter={16}>
              <Col span={12}>

                
              <Form.Item label="Vai Trò Hiện Tại"
                name="">
                

                </Form.Item>
              <Form.Item label="Vai Trò"
                name="role">
                <Select
                value={inputs.role || ''} onChange={handleChange} 
                >
                  <Select.Option  value="dev">DEV</Select.Option>
                  <Select.Option value="prod">PROD</Select.Option>
                  <Select.Option value="edit">EDITTER</Select.Option>
                  <Select.Option value="write">WRITTER</Select.Option>
                  <Select.Option value="admin">ADMIN</Select.Option>
                </Select>
              </Form.Item>
              </Col>
              <Col span={12}>
              
              </Col>
              <Col span={12}>
              <Form.Item>
                  <Button type="primary"  htmlType="submit">
                    Cập Nhật
                  </Button>
                </Form.Item>
                </Col>
            </Row>
          </Form>
        </Drawer>
        <Spin spinning={loading2}>
        <ToastContainer />

        <Table rowSelection={rowSelection} columns={columns} onChange={handletablechange2}  pagination={pagination}  dataSource={product} rowKey={"id_pro"}/>
            </Spin>
      </div>
    );
   

   
  }
 
 
export default AllProduct;
