<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductClientController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\ProductDetailsController;
use App\Http\Controllers\TrashController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [AuthController::class,'login']);
Route::post('register', [AuthController::class,'register']);
//Route api admin 
Route::group(['middleware'=>'api'],function(){
    Route::post('logout', [AuthController::class,'logout']);
    Route::post('refresh', [AuthController::class,'refresh']);
    Route::post('me', [AuthController::class,'me']);
});


//JWT middleware ====

Route::group([

    'middleware' => ['api'] , 'prefix' => 'auth',
   
], function ($router) {

     Route::resource('products',ProductController::class);



});

//========================

//Route api admin:auth
// Route::group(['middleware'=> ['auth:api', 'role:admin2']], function()
// {
//     Route::resource('products',ProductController::class);
// }
// );
// Route::group(['middleware'=>'api','role:admin2'], function ()
// {
// Route::resource('products',ProductController::class);
// });

// Route::resource('products',ProductController::class);



Route::resource('cccc',ProductController::class);
Route::resource('trash',TrashController::class);
Route::resource('users',UserController::class);
// Route::resource('users_role',[UserController::class,'rolever']);
Route::resource('productdetails',ProductDetailsController::class);
// Route::post('productdetails',ProductDetailsController::class,'update');
Route::resource('users_role',UserRoleController::class);

Route::get('detailsclient/{id}',[ProductDetailsController::class,'indexclient']); 
Route::get('CreateDetails/{id}',[ProductDetailsController::class,'check_details']);


Route::group(['middleware' =>'api', ['role: clientdev']], function () {
    Route::get('productclient',[ProductClientController::class,'indexclient']);
});