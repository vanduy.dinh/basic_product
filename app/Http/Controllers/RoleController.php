<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Product;
use Validator;


class RoleController extends Controller
{
   

    public function index(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name_pro'=>'required|max:255',
            'phanloai'=>'required|max:255',
            'mota'=>'required|max:255',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }else{
        $prod = Product::where("id_pro",$id_pro);
        $prod->update([
            'name_pro'=>$request->name_pro,
            'phanloai'=>$request->phanloai,
            'mota'=>$request->mota,
            'icon'=>$request->icon,
        ]);
        return response()->json('update thanh cong');
    }
    return response()->json("du lieu nhap khong dung");
    }  
    
    
    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_pro)
    {
        //
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_pro)
    {
        
        // return response()->json(Product::where("id_pro",$id_pro)->first());
        return response()->json(Product::where("id_pro",$id_pro)->first());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request,$id_pro)
    {
        
        // return $input = $request->all();
      
        $validator = Validator::make($request->all(), [
            'name_pro'=>'required|max:255',
            'phanloai'=>'required|max:255',
            'mota'=>'required|max:255',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }else{
        $prod = Product::where("id_pro",$id_pro);
        $prod->update([
            'name_pro'=>$request->name_pro,
            'phanloai'=>$request->phanloai,
            'mota'=>$request->mota,
            'icon'=>$request->icon,
        ]);
        return response()->json('update thanh cong');
    }
    return response()->json("du lieu nhap khong dung");

    }


    public function destroy(Request $request,$id_pro)
    {
        Product::where("id_pro",$id_pro)->delete();
        return response()->json('success');
    }

}