<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Product;
use Validator;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthController;



class ProductClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    // public function __construct()
    // {
    //     $this->middleware(['role:clientdev']);
    // }
    private Factory $auth;
    

    public function index()
    {
        $phantrang2 = Product::orderBy('id_pro','DESC')->paginate(4);
        return response()->json($phantrang2);


    }   
    public function indexclient()
    {
        $phantrang2 = Product::orderBy('id_pro','DESC')->paginate(44);
        return response()->json($phantrang2);
        

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //kiem tra
        $validator = Validator::make($request->all(), [
            'name_pro'=>'required',
            'icon'=>'required',
            'phanloai'=>'required',
            'mota'=>'required|max:10',
        ]);

        //check kiem tra -> add data
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);


            }else{
                try {
                    Product::create([
                        'name_pro'=>$request->name_pro,
                        'phanloai'=>$request->phanloai,
                        'mota'=>$request->mota,
                        'icon' =>$request->icon,
                    ]);
                    return response()->json('them thanh cong');
                } catch (\Throwable $th) {
                    return response()->json('them khong thanh cong');
                }
                
            }
         return response()->json("data khong dung yeu cau ");


         //Back
          //kiem tra
        // $validator = Validator::make($request->all(), [
        //     'name_pro'=>'required',
        //     'icon'=>'required',
        //     'phanloai'=>'required',
        //     'mota'=>'required|max:10',
        // ]);

        // //check kiem tra -> add data
        //     if($validator->fails()){
        //         return response()->json($validator->errors()->toJson(),400);


        //     }else{
        //         try
        //         Product::create([
        //                         'name_pro'=>$request->name_pro,
        //                         'phanloai'=>$request->phanloai,
        //                         'mota'=>$request->mota,
        //                         'icon' =>$request->icon,
        //                     ]);
        //                     return response()->json('them thanh cong');
        //     }
        //  return response()->json("data khong dung yeu cau ");
           
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_pro)
    {
        //
    }
    // public function fetchOne(Request $request , $id_pro)
    // {
    //     $data = request()->all();
    //     $id = isset($data['id']) ? intval($data['id']) : (isset($id) ? $id : "");
    //     $dataJson = $this->AdsourcesRepository->show($id);
    //     return $this->response($dataJson ? $dataJson : null, "Succcess!", 200);
    // }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_pro)
    {
        
        return response()->json(Product::where("id_pro",$id_pro));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_pro)
    {
        $prod = Product::where("id_pro",$id_pro);
        $prod->update([
            'name_pro'=>$request->name_pro,
            'phanloai'=>$request->phanloai,
            'mota'=>$request->mota,
        ]);
        return response()->json('update thanh cong');
    }
    public function destroy(Request $request,$id_pro)
    {
        Product::where("id_pro",$id_pro)->delete();
        return response()->json('success');
    }

}