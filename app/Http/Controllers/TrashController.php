<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Product;
use App\Models\Image;
use Validator;
use App\Models\Productdetails;


class TrashController extends Controller
{
    /**=
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    return response()->json(Productdetails::where('active','0')->get());
    // $phantrang2 = Productdetails::where('active','1')->paginate(4);
    //     return response()->json($phantrang2);
    }

    public function softdelete(){
    }



    public function indexclient($id)
    {   
        $user = User::find($id);

        
            if( $user->hasRole(8)){
                return response()->json(Productdetails::where('version','prod')->get());
            }  else{
                if( $user->hasRole(7)){
                    return response()->json(Productdetails::where('version','dev')->get());
                }else{
                    return response()->json('khong co quyen xem');
                }
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'link' => 'required',
            'version' => 'required',
            'userpass' => 'required',
            'image' => 'required|image'
        ]);
        if( $validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }
        else{
            if($request->hasFile('image')){
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalExtension();
                $image->move('images/',$name);
                Productdetails::create([
                    'link'=>$request->link,
                    'version'=>$request->version,
                    'userpass'=>$request->userpass,
                    'product_id'=>$request->product_id,
                    'image'=>$name,
                    'active'=>'1',
                ]);
                return response()->json(["message"=>'Upload thanh cong']);
            }
            return response()->json('Error : upload that bai');
        }

        
     
        /**BACK UP */
        // if($request->hasFile('image')){
        //     $image = $request->file('image');
        //     $name = time().'.'.$image->getClientOriginalExtension();
        //     $image->move('images/',$name);
        //     Productdetails::create([
        //         'link'=>$request->link,
        //         'version'=>$request->version,

        //         'userpass'=>$request->userpass,
        //         'product_id'=>$request->product_id,
        //         'image'=>$name,
        //     ]);
          
        //     return response()->json(["message"=>'Upload successfully']);
        // }

        // return response()->json('plz try again');
     
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_prodetail )
    {
        return response()->json(Productdetails::where("id_prodetail",$id_prodetail)->first());
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_prodetail)
    {
        $user = Productdetails::where("id_prodetail",$id_prodetail)->first();
        $user->update([
            'active'=>'1',
        ]);
        return response()->json('update thanh cong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_prodetail )
    {
        Productdetails::where("id_prodetail",$id_prodetail )->first()->delete();
        // $delete = Productdetails::where("id_prodetail",$id_prodetail);
        // $delete->update([
        //     'active' => '0',
        // ]);
        return response()->json('success');
        // return response()->json('success');
    }
    public function laydev($id_prodetail){
        dd($id_prodetail);
        return response()->json('success');
    }
    public function check_details($id_prodetail){
        return response()-> json(Productdetails::where("",$id_prodetail)->get());
            // $prod = Productdetails::where("product_id",$id);
            // if($prod != null ){
            //     return response()-> json(Productdetails::where("product_id",$id));
            // }
            // return response()->json('dinh bay  product');

    }
    
    
}   