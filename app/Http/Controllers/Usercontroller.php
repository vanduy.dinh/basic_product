<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class UserController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        //Role::create(['name'=>'admin3']);
        // $permission = Permission::create(['name' => 'delete genre']);

        // $role = Role::find(1);
        // $permission = Permission::find(2);
        // $role->givePermissionTo($permission);
        
        //TEST PErmission

        //  $role = Role::find(5);
        //  $permission = Permission::find(2);
        //  $user = User::find(18);

        //  $role->givePermissionTo($permission);
       

        //  $role->givePermissionTo($permission);
        // $user->assignRole(['publisher']);
        // if($user->hasRole('admin2')){
        //     $phantrang = User::orderBy('id','DESC')->paginate(4);
        //     return response()->json($phantrang);
        // }else{
        //   return response()->json(" user khong co quyen ");

        // }

        
        //  $user->removeRole('admin2');


        //TEST 

    //   $user = User::find(2);
    //   $user->givePermissionTo('add articles 2');


///Back up

 $phantrang = User::orderBy('id','DESC')->paginate(4);
        return response()->json($phantrang);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> bcrypt($request->password)
        ]);
        
        return response()->json('successfully created');
    }

    /**
     * Display the specified resource.
     * Display the specified resourcce .
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_pro)
    {   
        $user = User::find($id_pro);
        if($user->hasRole(11)){
            return response()->json(User::whereId($id_pro)->first());

        }
        return response()->json("no permission");

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_pro)
    {
        $user = User::where('id',$id_pro)->first();
        $user->update([
            'name'=>$request->name,
            'password'=>$request->password,
            'email'=>$request->email,
            'phanquyen'=>$request->phanquyen,
        ]);
        return response()->json('update thanh cong');
    }
    public function rolever(Request $request, $id_pro)
    { $user = User::where('id',$id_pro)->first();

        $user->update([
            'name'=>$request->name,
            
        ]);
        return response()->json('update thanh cong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::whereId($id)->first()->delete();
        return response()->json('success');
    }
    
}