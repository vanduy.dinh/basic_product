<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Auth;


use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;



class UserRoleController extends Controller     

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
       
         // $this->middleware(['auth:api','role:9|11'], ['except' => ['index', 'store']]  );
         // $this->middleware(['auth:api','role:10'], ['except' => [ 'store']]  );
         
     }
    public function index()
    {
       
        //Role::create(['name'=>'admin3']);
        // $permission = Permission::create(['name' => 'delete genre']);

        // $role = Role::find(1);
        // $permission = Permission::find(2);
        // $role->givePermissionTo($permission);
        
        //TEST PErmission

        //  $role = Role::find(5);
        //  $permission = Permission::find(2);
        //  $user = User::find(18);

        //  $role->givePermissionTo($permission);
       

        //  $role->givePermissionTo($permission);
        // $user->assignRole(['publisher']);
        // if($user->hasRole('admin2')){
        //     $phantrang = User::orderBy('id','DESC')->paginate(4);
        //     return response()->json($phantrang);
        // }else{
        //   return response()->json(" user khong co quyen ");

        // }

        
        //  $user->removeRole('admin2');


        //TEST 

    //   $user = User::find(2);
    //   $user->givePermissionTo('add articles 2');


///Back up

 $phantrang = User::orderBy('id','DESC')->paginate(4);
        return response()->json($phantrang);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> bcrypt($request->password)
        ]);
        return response()->json('successfully created');
    }

    /**
     * Display the specified resource.
     * Display the specified resourcce .
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_pro)
    {
        if($id_pro == 33 ){
            return response()->json(User::whereId($id_pro)->first());

        }
        return response()->json("khong co");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_pro)
    {
        $id = $request->name;
     $checkadmin = User::find($id);
     if($checkadmin->hasRole('admin')){
        $user = User::find($id_pro);
        if($request->role == "prod") {
           $role = Role::Find(8);
           $permission = Permission::find(7);
           $role->revokePermissionTo(6);
           $role->givePermissionTo($permission);
           $user->assignRole($role);
            return response()->json("prod ");
        }
        else if($request->role == "dev"){
            $role = Role::Find(7);
            $permission = Permission::find(6);
            // $role->revokePermissionTo(7);
            $user ->removeRole(8);
            $role->givePermissionTo($permission);
            $user->assignRole($role);
            return response()-> json( "dev");
        }
        
        else if($request->role == "edit"){
            $role = Role::Find(10);
            $permission = Permission::find(9);   
            $role->givePermissionTo($permission);
            $user->assignRole($role);
            return response()-> json( "edit");
        }
        
        
        else if($request->role == "write"){
            $role = Role::Find(9);
            $permission = Permission::find(8);
            // $role->revokePermissionTo(7);
            $role->givePermissionTo($permission);
            $user->assignRole($role);
            return response()-> json( "write");
        }
        else if($request->role == "admin"){

            if($checkadmin->hasRole('super_admin')){ 
            
                if( $user->hasRole(7)){
                    $role = Role::Find(11);
                    $permission = Permission::find(6);
                    $user ->removeRole(7);
                    $role->givePermissionTo($permission);
                    $user->assignRole($role);
                     return response()->json("admin ");
                }else{
                    if( $user->hasRole(8)){
                        $role = Role::Find(11);
                        $permission = Permission::find(6);
                        $user ->removeRole(8);
                        $role->givePermissionTo($permission);
                        $user->assignRole($role);
                         return response()->json("admin ");
                    }
                }
            
            }
            else{
                return response()->json("khong co quyen super_admin");
            }

   
           


        }
    return response()->json("khong cap quyen dc ");

     }
     return response()->json("khong co quyen super_admin");
        
      
      
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::whereId($id)->first()->delete();
        return response()->json('success');
    }

}