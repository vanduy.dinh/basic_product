<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Product;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;


class ProductController extends Controller
{
    use HasRoles;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function __construct()
    {
      
        // $this->middleware(['auth:api','role:9|11'], ['except' => ['index', 'store']]  );
        // $this->middleware(['auth:api','role:10'], ['except' => [ 'store']]  );
            $this->middleware('auth:api', ['except' => ['index','store','edit','destroy', 'update']]  );
        
    }
    public function index()
    {
        $phantrang2 = Product::orderBy('id_pro','DESC')->paginate(4);
        return response()->json($phantrang2);
    }  
    public function thungrac()
    {
        $phantrang2 = Product::orderBy('id_pro','DESC')->paginate(4);
        return response()->json($phantrang2);
    }
    
    public function indexclient()
    {
        $phantrang2 = Product::orderBy('id_pro','DESC')->paginate(44);
        return response()->json($phantrang2);
        // $user = User::find(13);
        // if( $user->hasRole('clientdev')){
        //      $phantrang2 = Product::orderBy('id_pro','DESC')->paginate(44);
        //     // $phantrang2 = Product::where('id_pro',)
        //      return response()->json($phantrang2);
        // }else{
        //     return response()->json("khong co quyen dev");
        // }
       // return response()->json(Product::latest()->paginate(44));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        

        $id = $request->id_user;
        $user = User::find($id);
        if($user->hasRole(9)){
         
            $validator = Validator::make($request->all(), [
                'name_pro'=>'required',
                'icon'=>'required',
                'phanloai'=>'required',
                'mota'=>'required|max:10',
            ]);
            //check kiem tra -> add data
                if($validator->fails()){
                    return response()->json($validator->errors()->toJson(),400);
                }else{
                    Product::create([
                                    'name_pro'=>$request->name_pro,
                                    'phanloai'=>$request->phanloai,
                                    'mota'=>$request->mota,
                                    'icon' =>$request->icon,
                                ]);
                                return response()->json('them thanh cong');
                }
             return response()->json("data khong dung yeu cau ");
                
        }
        return response()->json("user khong co quyen nay");
        



           
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_pro)
    {
        //
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_pro)
    {
        
        // return response()->json(Product::where("id_pro",$id_pro)->first());
        return response()->json(Product::where("id_pro",$id_pro)->first());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request,$id_pro)
    {
        $id = $request->id_user;
        $user = User::find($id);
        if($user->hasPermissionTo("edit")){
            $validator = Validator::make($request->all(), [
                'name_pro'=>'required|max:255',
                'phanloai'=>'required|max:255',
                'mota'=>'required|max:255',
                'icon'=>'required'
            ]);
            if($validator->fails()){
                return response()->json("error data");
            }else{
            $prod = Product::where("id_pro",$id_pro);
            $prod->update([
                'name_pro'=>$request->name_pro,
                'phanloai'=>$request->phanloai,
                'mota'=>$request->mota,
                'icon'=>$request->icon,
            ]);
            return response()->json('update thanh cong');
        }
        return response()->json("du lieu nhap khong dung");

        }

        return response()->json("user khong co quyen");

        

    }


    public function destroy(Request $request,$id_pro)
    {
        Product::where("id_pro",$id_pro)->delete();
        return response()->json('success');
    }

}