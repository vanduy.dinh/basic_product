<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelshasPer extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'role_id','model_type' , 'model_id' ,
    ];
    protected $primaryKey = 'role_id';
    protected $table = 'model_has_roles';
}
